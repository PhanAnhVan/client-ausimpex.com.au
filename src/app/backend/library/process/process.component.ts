import { Component, OnInit, OnDestroy } from "@angular/core";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { uploadFileService } from "../../../services/integrated/upload.service";
import { Globals } from "../../../globals";

@Component({
    selector: "app-process",
    templateUrl: "./process.component.html",
    styleUrls: ["./process.component.css"],
})
export class ProcessComponent implements OnInit, OnDestroy {
    fm: FormGroup;
    public connect;
    public listgrouplibrary: any;
    public token: any = {
        process: "set/library/process",
        getrow: "get/library/getrow",
        getgrouplibrary: "get/pages/grouptype",
    };

    public images = new uploadFileService();
    public id: number;

    constructor(
        public fb: FormBuilder,
        public router: Router,
        public toastr: ToastrService,
        public globals: Globals,
        private routerAct: ActivatedRoute
    ) {
        this.routerAct.params.subscribe((params) => {
            this.id = +params["id"];
        });
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response["token"]) {
                case "getrowLibrary":
                    let data: any = response["data"];
                    const imagesConfig = {
                        path: this.globals.BASE_API_URL + "public/library/",
                        data: +data.type == 1 ? data.value : "",
                    };
                    this.images._ini(imagesConfig); //khoi tao hinh anh
                    this.fmConfigs(data);
                    break;
                case "processaddlibrary":
                    let type =
                        response.status == 1 ? "success" : response.status == 0 ? "warning" : "danger";
                    this.toastr[type](response.message, type, { closeButton: true });
                    if (response.status == 1) {
                        setTimeout(() => {
                            this.router.navigate(["admin/library/get-list"]);
                        }, 2000);
                    }
                    break;

                case "getgrouplibrary":
                    this.listgrouplibrary = response["data"];
                    break;
                default:
                    break;
            }
        });
    }

    ngOnInit() {
        if (this.id && this.id != 0) {
            this.globals.send({
                path: this.token.getrow,
                token: "getrowLibrary",
                params: { id: this.id },
            });
        } else {
            const imagesConfig = {
                path: this.globals.BASE_API_URL + "public/library/",
            };
            this.images._ini(imagesConfig);
            this.fmConfigs();
        }

        this.globals.send({ path: this.token.getgrouplibrary, token: "getgrouplibrary", params: { type: 5 } });
    }
    ngOnDestroy() {
        this.connect.unsubscribe();
    }
    fmConfigs(item: any = "") {
        item = typeof item === "object" ? item : { status: 1, type: 1 };
        this.fm = this.fb.group({
            id: item.id ? item.id : "",

            name: [item.name ? item.name : "", [Validators.required]],

            images: item.images ? item.images : "",

            value: item.value ? item.value : "",

            orders: +item.orders ? +item.orders : 0,

            page_id: [item.page_id ? item.page_id : "", [Validators.required]],

            type: +item.type ? +item.type : "",

            description: item.description ? item.description : "",

            status: item.status && item.status == 1 ? true : false,
        });

        const imagesConfig = {
            path: this.globals.BASE_API_URL + "public/library/",
            data: item.images ? item.images : "",
        };

        this.images._ini(imagesConfig);
    }
    changeUrlVideo(e) {
        e.target.value = e.target.value.replace("watch?v=", "embed/");
    }
    onSubmit() {
        var data: any = this.fm.value;


        data.images = this.images._get(true);

        data.status = data.status == true ? 1 : 0;
        this.globals.send({ path: this.token.process, token: "processaddlibrary", data: data, });
    }
}
