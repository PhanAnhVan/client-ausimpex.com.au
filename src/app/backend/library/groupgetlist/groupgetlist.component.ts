import {
    Component,
    OnInit,
    TemplateRef,
    ViewContainerRef,
    Output,
    EventEmitter,
    OnDestroy,
} from "@angular/core";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { TableService } from "../../../services/integrated/table.service";
import { ToastrService } from "ngx-toastr";
import { Globals } from "../../../globals";
import { AlertComponent } from "../../modules/alert/alert.component";

@Component({
    selector: "app-groupgetlist",
    templateUrl: "./groupgetlist.component.html",
    styleUrls: ["./groupgetlist.component.css"],
})
export class GroupgetlistComponent implements OnInit {
    private id: number;
    public show;
    private name: any;
    modalRef: BsModalRef;
    @Output("filter") filter = new EventEmitter();
    public connect;
    public token: any = {
        getlist: "get/pages/grouptype",
    };
    private cols = [
        { title: "lblStt", field: "orders", show: false, filter: true },
        { title: "libraryGroup.title", field: "name", show: true, filter: true },
        { title: "libraryGroup.count", field: "count_library", show: true, filter: true },
    ];
    public table = new TableService();
    constructor(
        private modalService: BsModalService,
        public globals: Globals,
        public toastr: ToastrService,
        vcr: ViewContainerRef
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getlistgroup":
                    this.table._concat(res.data, true);

                    break;
                case "remove":
                    let type = res.status == 1 ? "success" : res.status == 0 ? "warning" : "danger";
                    this.toastr[type](res.message, type);
                    if (res.status == 1) {
                        setTimeout(() => {
                            this.onClose();
                        }, 1000);
                        this.table._delRowData(this.id);
                    }
                    break;
                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.globals.send({ path: this.token.getlist, token: 'getlistgroup', params: { type: 5 } });
        this.table._ini({ cols: this.cols, data: [], keyword: "librarygroup" });
    }
    ngOnDestroy() {
        this.connect.unsubscribe();
    }
    onClose() {
        this.modalRef.hide();
    }

    onRemove(id: number, name: any) {
        this.id = id;
        this.modalRef = this.modalService.show(AlertComponent, {
            initialState: { messages: "libraryGroup.remove", name: name },
        });
        this.modalRef.content.onClose.subscribe((result) => {
            if (result == true) {
                this.globals.send({
                    path: this.token.remove,
                    token: "remove",
                    params: { id: id },
                });
            }
        });
    }
    onCheckItem(item) {
        item.check = item.check == true ? false : true;
        this.filter.emit(item.id);
    }
    onSubmit() {
        this.globals.send({
            path: this.token.remove,
            token: "remove",
            params: { id: this.id },
        });
    }
}
