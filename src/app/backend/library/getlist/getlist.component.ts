import {
    Component,
    OnInit,
    Input,
    ViewContainerRef,
    OnDestroy,
    OnChanges,
    SimpleChanges,
} from "@angular/core";
import { TableService } from "../../../services/integrated/table.service";
import { ToastrService } from "ngx-toastr";
import { Globals } from "../../../globals";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { AlertComponent } from "../../modules/alert/alert.component";

@Component({
    selector: "app-getlist",
    templateUrl: "./getlist.component.html",
    styleUrls: ["./getlist.component.css"],
})
export class GetlistComponent implements OnInit, OnChanges, OnDestroy {
    @Input("filter") filter: any;
    @Input("change") change: any;

    private cols = [
        { field: "id", filter: true },
        { title: "lblStt", field: "index", show: true },
        { title: "library.lblSubject", field: "name", show: true, filter: true },
        { title: "library.lblValue", field: "images", show: true, filter: true },
        // { title: "library.lblValue", field: "video", show: true, filter: true },
        { title: "library.lblType", field: "type", show: true, filter: true },
        {
            title: "library.maker_date",
            field: "maker_date",
            show: true,
            filter: true,
        },
        { title: "lblAction", field: "action", show: true },
    ];
    private id: number;
    modalRef: BsModalRef;
    public connect;
    public token: any = {
        getlist: "get/library/getlist",
        remove: "set/library/remove",
        getlistGroup: "get/librarygroup/getlist",
    };
    constructor(
        private modalService: BsModalService,
        public globals: Globals,
        public cwstable: TableService,
        public toastr: ToastrService,
        vcr: ViewContainerRef
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getlistlibrary":
                    this.cwstable.sorting = { field: "maker_date", sort: "DESC", type: "", };

                    this.cwstable._concat(res.data, true);

                    break;
                case "removelibrary":
                    let type = res.status == 1 ? "success" : res.status == 0 ? "warning" : "danger";
                    this.toastr[type](res.message, type);
                    if (res.status == 1) {
                        this.cwstable._delRowData(this.id);
                        this.globals.send({ path: this.token.getlistGroup, token: "getlistgroup" });
                    }
                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.globals.send({ path: this.token.getlist, token: "getlistlibrary" });
        this.cwstable._ini({
            cols: this.cols,
            data: [],
            keyword: "library",
            count: 50,
        });
    }

    onRemove(item) {
        this.id = +item.id;
        this.modalRef = this.modalService.show(AlertComponent, {
            initialState: { messages: "library.removelibrary", name: item.name },
        });
        this.modalRef.content.onClose.subscribe((result) => {
            if (result == true) {
                this.globals.send({
                    path: this.token.remove,
                    token: "removelibrary",
                    params: { id: item.id, value: item.type == 1 ? item.value : "" },
                });
            }
        });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    ngOnChanges(e: SimpleChanges) {
        if (typeof this.filter === "object") {
            let value = Object.keys(this.filter);
            if (value.length == 0) {
                this.cwstable._delFilter("page_id");
            } else {
                this.cwstable._setFilter("page_id", value, "in", "number");
            }
        }
    }
}
