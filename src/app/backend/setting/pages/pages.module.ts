import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GetListTypeComponent } from './get-list-type/get-list-type.component';
import { ProcessTypeComponent } from './process-type/process-type.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap';
import { CKEditorModule } from 'ckeditor4-angular';

export const routes: Routes = [

    { path: '', redirectTo: 'get-list' },
    { path: 'get-list', component: GetListTypeComponent },
    { path: 'insert', component: ProcessTypeComponent },
    { path: 'update/:id', component: ProcessTypeComponent },
];
@NgModule({
    declarations: [
        GetListTypeComponent,
        ProcessTypeComponent,],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        AlertModule.forRoot(),
        CKEditorModule
    ]
})
export class SetiingsPagesModule { }
