import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap';
import { BrandListComponent } from './get-list/get-list.component';
import { BrandProcessComponent } from './process/process.component';

export const routes: Routes = [

    { path: '', redirectTo: 'get-list' },
    { path: 'get-list', component: BrandListComponent },
    { path: 'insert', component: BrandProcessComponent },
    { path: 'update/:id', component: BrandProcessComponent },
];

@NgModule({
    declarations: [BrandListComponent, BrandProcessComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        AlertModule.forRoot()
    ]
})
export class BrandModule { }
