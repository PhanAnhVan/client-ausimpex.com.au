import { Component, OnInit, Input, SimpleChanges, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { TableService } from '../../../services/integrated/table.service';
import { ToastrService } from 'ngx-toastr';
import { AlertComponent } from '../../modules/alert/alert.component'
import { Router } from '@angular/router';
import { Globals } from '../../../globals';

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html',
    styleUrls: ['./getlist.component.css']
})


export class GetlistComponent implements OnInit, OnDestroy {

    private id: number = 0;
    private connect;
    public show: any;

    public token: any = {
        getlist: "get/products/getlist",
        remove: "set/products/remove"
    }
    @Input("filter") filter: any;
    @Input("change") change: any;
    modalRef: BsModalRef;
    private cols = [
        { title: 'lblStt', field: 'index', show: false },
        { title: 'lblImages', field: 'images', show: true },
        { title: 'products.name', field: 'name', show: true, filter: true },
        { title: 'products.price', field: 'price', show: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: 'lblAction', field: 'action', show: true },
    ];

    public tableproduct = new TableService();

    constructor(
        private modalService: BsModalService,
        public toastr: ToastrService,
        public router: Router,
        public globals: Globals
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getlistproduct":
                    this.tableproduct.sorting = { field: "maker_date", sort: "DESC", type: "" };
                    this.tableproduct._concat(res.data, true);
                    break;
                case "removeproduct":
                    let type = (res.status === 1) ? "success" : (res.status === 0 ? "warning" : "danger");
                    this.toastr[type](res['message'], type);
                    if (res['status'] == 1) {
                        this.tableproduct._delRowData(this.id);
                    }
                    break;
                case 'updatePriceSeo':
                    let message = (res.status === 1) ? "success" : (res.status === 0 ? "warning" : "danger");
                    this.toastr[message](res.message, message);
                    if (res.status === 1) {
                        this.price.skip = false
                    }
                    break;
                default:
                    break;
            }
        });


    }

    ngOnInit() {
        this.getlist();
    }
    ngOnDestroy() {
        this.connect.unsubscribe();
    }
    getlist = () => {
        this.globals.send({ path: this.token.getlist, token: 'getlistproduct' });
        this.tableproduct._ini({ cols: this.cols, data: [], count: 50 });
    }
    ngOnChanges(e: SimpleChanges) {
        if (typeof this.filter === 'object') {
            let value = Object.keys(this.filter);
            if (value.length == 0) {
                this.tableproduct._delFilter('page_id');
            } else {
                this.tableproduct._setFilter('page_id', value, 'in', 'number');
            }
        }
    }
    onClose() {
        this.modalRef.hide();
    }
    onRemove(item: any): void {
        this.id = item.id;
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'products.remove', name: item.name } });
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'removeproduct', params: { id: item.id } });
            }
        });
    }
    public price = {

        skip: false,
        vaild: true,
        index: 0,
        token: 'set/products/update',
        onChange: (item) => {
            this.price.skip = true;
            this.price.index = item.id;
            if (!this.price.checkPrice(item.price_buy, item.price_sale)) {
                this.price.vaild = false
            }
        },

        checkPrice: (price_buy, price_sale) => {
            let item = this.price.get(price_buy, price_sale);
            return +item.price_sale > 0 && +item.price_buy <= +item.price_sale ? true : false
        },

        updatePrice: (item) => {
            const obj = this.price.get(item.price_buy, item.price_sale);
            this.globals.send({ path: this.price.token, token: 'updatePriceSeo', data: obj, params: { id: item.id || 0 } });
        },

        get: (price_buy, price_sale) => {
            let item = {
                price_sale: +price_sale.toString().split('.').join(''),
                price_buy: +price_buy.toString().split('.').join('')
            }
            return item;
        }
    }

}

