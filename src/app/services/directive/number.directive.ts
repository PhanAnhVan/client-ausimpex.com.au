import { Directive, ElementRef, HostListener, OnChanges, SimpleChanges } from '@angular/core';

@Directive({
    selector: '[symbolNumber]'
})
export class NumberDirective implements OnChanges {
    constructor(public el: ElementRef) {

        setTimeout(() => {
            this.changes();
        }, 500);
    }
    ngOnChanges(e: SimpleChanges) {
        setTimeout(() => {
            this.changes();
        }, 500);
    }
    @HostListener("change") changesValue() {
        setTimeout(() => {
            this.changes();
        }, 500);
    }
    @HostListener("keyup") keupValue() {
        setTimeout(() => {
            this.changes();
        }, 300);
    }
    changes = () => {
        let value = this.el.nativeElement.value.toString().replace(/[^0-9.-]+/g, '').split('.').join('');;
        this.el.nativeElement.value = new Number(value).toLocaleString('vn').split(',').join('.')   
    }
}
