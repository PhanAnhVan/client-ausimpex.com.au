import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { PaginationModule } from 'ngx-bootstrap';
import { TimepickerModule, BsDatepickerModule, TabsModule } from 'ngx-bootstrap';

import { CarouselModule as OwlCarouselModule } from "ngx-owl-carousel-o";
import { Ng5SliderModule } from 'ng5-slider';

import { LazyLoadImageModule, intersectionObserverPreset } from 'ng-lazyload-image';
import { SanitizeHtmlPipe } from './sanitizeHtml.pipe';
import { BindSrcDirective } from '../services/directive/bindSrc.directive';

import { FrontendComponent } from './frontend.component';

import { CommentComponent } from './modules/comment/comment.component';
import { SlideComponent } from './modules/slide/slide.component';
import { HeaderComponent } from './modules/header/header.component';
import { FooterComponent } from './modules/footer/footer.component';
import { MenuComponent } from './modules/menu/menu.component';
import { MenuMobileComponent } from './modules/menu-mobile/menu-mobile.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { BoxContentComponent } from './modules/box-content/box-content.component';
import { ListPageComponent } from './page/list/page.component';
import { DetailPageComponent } from "./page/detail-page/detail-page.component";
import { BoxGalleryComponent } from './modules/box-gallery/box-gallery.component';
import { GalleryComponent } from './gallery/gallery.component';
import { SearchComponent } from './search/search.component';
import { BoxProductComponent } from './modules/box-product/box-product.component';
import { ProductListComponent } from './products/list/list.component';
import { DetailProductComponent } from './products/detail-product/detail-product.component';


const appRoutes: Routes = [
    {

        path: '', component: FrontendComponent,
        children: [
            { path: ':lang/home', redirectTo: '' },
            { path: ':lang/trang-chu', redirectTo: '' },
            { path: ':lang', component: HomeComponent },

            { path: ':lang/lien-he', component: ContactComponent },
            { path: ':lang/contact', component: ContactComponent },

            { path: ':lang/search', component: SearchComponent },
            { path: ':lang/search/:keywords', component: SearchComponent },

            { path: ':lang/:link', component: ListPageComponent },
            { path: ':lang/:parent_link/:link', component: ListPageComponent },
            { path: ':lang/:parent_links/:parent_link/:link', component: DetailPageComponent },
            { path: ':lang/:link_defaut/:parent_links/:parent_link/:link', component: DetailPageComponent },
        ]
    }
]

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        TabsModule.forRoot(),
        RouterModule.forChild(appRoutes),
        TimepickerModule.forRoot(),
        BsDatepickerModule.forRoot(),
        ModalModule.forRoot(),
        OwlCarouselModule,
        TypeaheadModule.forRoot(),
        PaginationModule.forRoot(),
        LazyLoadImageModule.forRoot({
            preset: intersectionObserverPreset
        }),
        Ng5SliderModule
    ],

    declarations: [
        FrontendComponent,
        HomeComponent,
        ContactComponent,
        HeaderComponent,
        FooterComponent,
        SlideComponent,
        ListPageComponent,
        MenuComponent,
        MenuMobileComponent,
        CommentComponent,
        BoxContentComponent,
        BoxProductComponent,
        SanitizeHtmlPipe,
        BindSrcDirective,
        DetailPageComponent,
        BoxGalleryComponent,
        GalleryComponent,
        SearchComponent,
        ProductListComponent,
        DetailProductComponent
    ],


})
export class FrontendModule { }
