import { Component, Input, OnInit } from '@angular/core';
import { Globals } from '../../../globals';
import { Router, ActivatedRoute } from '@angular/router';
import { TableService } from '../../../services/integrated/table.service';
import { LabelType, ChangeContext, Options } from 'ng5-slider';
import { PageChangedEvent } from "ngx-bootstrap/pagination";

@Component({
    selector: 'app-list-product',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ProductListComponent implements OnInit {

    @Input('data') data: any;
    @Input('parent_link') parent_link: any;
    @Input('group') group_id: any;
    private connect;
    public categories: any = [];
    public show: number = -1;
    public price: any = { min: 0, max: 0 };
    public collapsedMenu = []
    public openMobileFilter = false;
    private cols = [
        { title: 'Lọc theo tên: ', field: 'name', filter: true },
        { title: 'Lọc theo giá: ', field: 'price', filter: true, type: 'number' },

    ];
    minValue: number = 0;
    maxValue: number = 100;
    options: Options = {
        floor: 0,
        ceil: 100,
        translate: (value: number, label: LabelType): string => {
            switch (label) {
                case LabelType.Low:
                    return '<b style="font-size:14px">Min: ' + this.setnumber(value) + ' đ </b>';
                case LabelType.High:
                    return '<b style="font-size:14px">Max: ' + this.setnumber(value) + ' đ </b>'
                default:
                    return this.setnumber(value);
            }
        }
    };

    public cwstable = new TableService();

    constructor(
        public globals: Globals,
        public router: Router,
        public ActivatedRoute: ActivatedRoute

    ) { }

    ngOnInit() {
        this.cwstable._ini({
            data: [], cols: this.cols, keyword: "getProduct", count: this.Pagination.itemsPerPage, sorting: { field: "name", sort: "", type: "number" },
        });

        this.cwstable._concat(this.data, true);

        this.extract();

        this.show = this.data.length > 0 ? 1 : 0;
    }

    public Pagination = {
        maxSize: 5,
        itemsPerPage: 16,
        change: (event: PageChangedEvent) => {
            const startItem = (event.page - 1) * event.itemsPerPage;
            const endItem = event.page * event.itemsPerPage;
            this.cwstable.data = this.cwstable.cached.slice(startItem, endItem);
            window.scrollTo({
                top: 0, left: 0, behavior: 'smooth'
            });
        }
    }

    public extract = () => {
        let brand = {};
        let origin = {};
        let group = {};
        let price = { min: 0, max: 0 };
        if (this.cwstable.cachedList.length > 0) {
            price.min = (+this.cwstable.cachedList[0].price_sale > 0) ? +this.cwstable.cachedList[0].price_sale : +this.cwstable.cachedList[0].price;

        }

        this.cwstable.cachedList.reduce((n, o, i) => {
            if (o.brand_id && +o.brand_id > 0) {
                if (!brand[o.brand_id]) {
                    brand[o.brand_id] = { id: o.brand_id, name: o.brand_name, count: 0 };
                }
                if (brand[o.brand_id]) {
                    brand[o.brand_id].count = +brand[o.brand_id].count + 1;
                }
            }

            if (o.origin_id && +o.origin_id > 0) {
                if (!origin[o.origin_id]) {
                    origin[o.origin_id] = { id: o.origin_id, name: o.origin_name, count: 0 };
                }
                if (origin[o.origin_id]) {
                    origin[o.origin_id].count = +origin[o.origin_id].count + 1;
                }
            }

            if (o.page_id && +o.page_id > 0) {
                if (!group[o.page_id]) {
                    group[o.page_id] = { id: o.page_id, name: o.page_name, count: 0 };
                }
                if (group[o.page_id]) {
                    group[o.page_id].count = +group[o.page_id].count + 1;
                }
            }
            let p = (+o.price_sale > 0) ? +o.price_sale : +o.price;
            price.min = p < price.min ? p : price.min;
            price.max = p > price.max ? p : price.max;

            this.changeOptions({
                floor: 0,
                ceil: price.max
            })

            return n;
        }, {});

        this.brand.data = Object.values(brand);
        this.origin.data = Object.values(origin);
        this.categories = Object.values(group);
        this.price = price;

    }

    public brand = {
        data: [],
        value: [],
        filterBrand: (id) => {
            let token = "brand_id";
            let filter = this.cwstable._getFilter(token);
            let data = !filter.value ? {} : filter.value.reduce((n, o, i) => { n[o] = o; return n; }, {});
            if (data[id]) {
                this.brand.value = filter.value.filter(item => { return +item == id ? false : true; });
            } else {
                this.brand.value.push(id);
            }
            if (this.brand.value.length == 0) {
                this.cwstable._delFilter(token);
            } else {
                this.cwstable._setFilter(token, this.brand.value, 'in');
            }
        }
    }

    public origin = {
        data: [],
        value: [],
        filterOrigin: (id) => {
            let token = "origin_id";
            let filter = this.cwstable._getFilter(token);
            let data = !filter.value ? {} : filter.value.reduce((n, o, i) => { n[o] = o; return n; }, {});
            if (data[id]) {
                this.origin.value = filter.value.filter(item => { return +item == id ? false : true; });
            } else {
                this.origin.value.push(id);
            }
            if (this.origin.value.length == 0) {
                this.cwstable._delFilter(token);
            } else {
                this.cwstable._setFilter(token, this.origin.value, 'in');
            }
        }


    }

    public groupCategory = {
        data: [],
        value: [],
        filter: (id, skip = 0) => {
            let token = "page_id"
            let filter = this.cwstable._getFilter(token);
            let data = !filter.value ? {} : filter.value.reduce((n, o) => {
                n[o] = o;
                return n;
            }, {});

            if (data[id]) {
                this.groupCategory.value = filter.value.filter(item => {
                    if (skip == 1) {
                        return true;
                    } else {
                        return +item == id ? false : true;
                    }
                });
            } else {
                this.groupCategory.value.push(id);
            }

            if (this.groupCategory.value.length == 0) {
                this.cwstable._delFilter(token);
            } else {
                this.cwstable._setFilter(token, this.groupCategory.value, 'in');
            }
        },
    }

    changeOptions(option) {

        const newOptions: Options = Object.assign({}, this.options);

        newOptions.ceil = option.ceil;

        newOptions.floor = option.floor;

        this.options = newOptions;

        this.minValue = option.floor;

        this.maxValue = option.ceil;
    }

    onUserChangeEnd(changeContext: ChangeContext): void {
        this.cwstable._setFilter('price', [changeContext.value, changeContext.highValue], 'between');
    }


    setnumber(Val) {

        let a = new Number(Val);

        return a.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
    }

    public collapseMenu(id) {
        if (this.collapsedMenu.includes(id) === true) {
            var index = this.collapsedMenu.indexOf(id);
            this.collapsedMenu.splice(index, 1);
        } else {
            this.collapsedMenu.push(id)
        }
    }

    setMenuMobile(open) {
        let list = document.getElementsByTagName('body')[0]
        this.openMobileFilter = open
        list.style.position = open ? 'fixed' : 'inherit'
    }
}
