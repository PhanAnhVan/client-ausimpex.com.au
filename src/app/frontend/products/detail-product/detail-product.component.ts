import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Globals } from "../../../globals";
import { OwlOptions, SlidesOutputData } from "ngx-owl-carousel-o";

@Component({
  selector: "app-detail-product",
  templateUrl: "./detail-product.component.html",
  styleUrls: ["./detail-product.component.css"],
})
export class DetailProductComponent implements OnInit {
  @Input("data") data: any;

  public connect;

  public item: any = {};

  //   public listImages = { data: [], cached: [], active: 0 };

  public activeSlides: SlidesOutputData;

  @ViewChild("owlLibrary", { static: false }) owlLibrary: any;

  public showOwl: boolean = false;

  public widthOwl: number;

  public selected: any;

  public width: number = 0;

  public link: string = "";

  public token: any = {
    getProductDetail: "api/getproductdetail",

    getRelatedProduct: "api/relatedproduct",
  };

  constructor(
    public route: ActivatedRoute,

    public router: Router,

    public toastr: ToastrService,

    public globals: Globals
  ) {
    this.width = document.body.getBoundingClientRect().width;
  }

  ngOnInit() {
    setTimeout(() => {
      this.renderHtml();
    }, 1000);

    let listimages =
      this.data.listimages && this.data.listimages.length > 5
        ? JSON.parse(this.data.listimages)
        : [];

    this.listImages.cached = listimages;

    if (this.data.images && this.data.images.length > 4) {
      this.listImages.cached = [this.data.images].concat(listimages);
    } else {
      this.data.images = listimages.length > 0 ? listimages[0] : "";
    }

    this.listImages.data = Object.values(this.listImages.cached);

    setTimeout(() => {
      this.widthOwl =
        document.getElementById("owl-carousel").offsetWidth /
          (this.width > 415 ? 5 : 2) -
        (this.width > 415 ? 15 : 20);
      this.showOwl = true;
    }, 300);
  }

  isActive(images, index) {
    this.item.images = images;
    this.listImages.active = index;
  }

  renderHtml = () => {
    let main = document.getElementById("contentDetail");

    if (main) {
      let el = main.querySelectorAll("table");

      if (el) {
        for (let i = 0; i < el.length; i++) {
          let div = document.createElement("div");
          div.className = "table-responsive table-bordered m-0 border-0";
          el[i].parentNode.insertBefore(div, el[i]);
          el[i].className = "table";
          el[i].setAttribute("class", "table");
          let cls = el[i].getAttribute("class");
          el[i];
          let newhtml = "<table class='table'>" + el[i].innerHTML + "</table>";
          el[i].remove();
          div.innerHTML = newhtml;
        }
      }

      let image = main.querySelectorAll("img");

      if (image) {
        for (let i = 0; i < image.length; i++) {
          let a = document.createElement("div");

          a.className = "images-deatil d-inline";

          image[i].parentNode.insertBefore(a, image[i]);

          let style = image[i].style.cssText;

          let width = image[i].width || 0;

          let heigth = image[i].width || 0;

          let src = image[i].currentSrc;

          let html =
            `<a  class="fancybox" data-fancybox="images-preview" data-thumbs="{&quot;autoStart&quot;:true}" href="` +
            src +
            `">
                                    <img class="mb-2" width="` +
            width +
            `px" heigth="` +
            heigth +
            `px" 
                                    src="` +
            src +
            `" style="` +
            style +
            `" alt="` +
            this.data.name +
            `" />
                                </a>`;
          image[i].remove();

          a.innerHTML = html;
        }
      }
    }
  };

  //   public library = {
  //     libraryOptions: {
  //       loop: true,
  //       autoplayTimeout: 8000,
  //       autoplaySpeed: 1500,
  //       mouseDrag: true,
  //       touchDrag: true,
  //       pullDrag: false,
  //       dots: false,
  //       navSpeed: 500,
  //       dotsData: true,
  //       items: 1,
  //       navText: [
  //         "<i class='fas fa-angle-left'></i>",
  //         "<i class='fas fa-angle-right'></i>",
  //       ],
  //       nav: true,
  //     },

  //     getPassedData: (data: SlidesOutputData) => {
  //       this.activeSlides = data;
  //       this.selected = data.slides[0] ? data.slides[0].id : "";
  //       this.data.images = this.selected;
  //       setTimeout(() => {
  //         this.library.onClickImageChild(this.selected + "-child");
  //       }, 200);
  //     },

  //     onClickImageChild: (id) => {
  //       let items: any = document.querySelectorAll(".img-child");
  //       for (let i = 0; i < items.length; i++) {
  //         items[i].style.opacity = 0.5;
  //         items[i].classList.remove("border");
  //         items[i].classList.remove("border-danger");
  //       }
  //       document.getElementById(id).style.opacity = "1";
  //       document.getElementById(id).classList.add("border");
  //       document.getElementById(id).classList.add("border-danger");
  //     },
  //   };

  related = {
    data: [],
    relatedOptions: {
      autoWidth: true,
      mouseDrag: true,
      touchDrag: true,
      pullDrag: true,
      margin: 10,
      responsive: {
        0: {
          items: 2,
        },
        740: {
          items: 3,
        },
        940: {
          items: 5,
        },
        1140: {
          items: 5,
        },
      },
      dots: false,
      nav: true,
      navText: [
        "<i class='fas fa-angle-left'></i>",
        "<i class='fas fa-angle-right'></i>",
      ],
    },
  };

  public listImages = {
    data: [],
    cached: [],
    active: 0,
    options: <OwlOptions>{
      autoplayTimeout: 8000,
      autoplaySpeed: 1500,
      mouseDrag: true,
      touchDrag: true,
      pullDrag: true,
      dots: false,
      navSpeed: 500,
      items: 5,
      navText: [
        "<img src='../../../../../assets/img/left-chevron.png' width='20' height='100%'>",
        "<img src='../../../../../assets/img/right-arrow.png' width='20' height='100%'>",
      ],
      nav: true,
    },
  };

  public library = {
    libraryOptions: <OwlOptions>{
      autoplayTimeout: 8000,
      autoplaySpeed: 1500,
      mouseDrag: true,
      touchDrag: true,
      pullDrag: false,
      dots: false,
      navSpeed: 500,
      items: 1,
      navText: [
        "<img src='../../../../../assets/img/left-chevron.png' width='20' height='100%'>",
        "<img src='../../../../../assets/img/right-arrow.png' width='20' height='100%'>",
      ],
      nav: true,
    },

    getPassedData: (data: SlidesOutputData) => {
      this.activeSlides = data;
      let selected = data.slides[0] ? data.slides[0].id : "";
      this.data.images = selected;
      setTimeout(() => {
        this.library.onClickImageChild(selected);
      }, 200);
    },

    onClickImageChild: (index) => {
      let id = index + "-child";
      let items: any = document.querySelectorAll(".img-child");
      if (items.length > 0) {
        for (let i = 0; i < items.length; i++) {
          items[i].style.opacity = 0.6;
          items[i].classList.remove("img_active");
        }
      }
      if (document.getElementById(id)) {
        document.getElementById(id).style.opacity = "1";
        document.getElementById(id).classList.add("img_active");
      }
      this.owlLibrary.to(index);
    },
  };
}
