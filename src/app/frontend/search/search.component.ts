import { Component, OnInit, OnDestroy, SimpleChanges, } from '@angular/core';
import { ToslugService } from '../../services/integrated/toslug.service';
import { TableService } from '../../services/integrated/table.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Globals } from '../../globals';
import { PageChangedEvent } from 'ngx-bootstrap';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css'],
    providers: [ToslugService, TableService],
})
export class SearchComponent implements OnInit, OnDestroy {

    private connect;

    public width: number = 0;

    public search: any = { 'name': '', show: -1 };

    public token: any = {

        getdata: "api/search",

    }

    public tableService = new TableService();

    public tableContent = new TableService();

    constructor(
        private route: ActivatedRoute,
        public globals: Globals,
        private router: Router,
        private toSlug: ToslugService,
        public cwstable: TableService,
    ) {
        this.width = document.body.getBoundingClientRect().width;

        this.tableService._ini({ data: [], keyword: 'getdataServiceSearch', count: this.Pagination.itemsPerPage, sorting: { field: "maker_date", sort: "DESC", type: "date" } });

        this.tableService.sorting.field = '';

        this.tableContent._ini({ data: [], keyword: 'getdataContentSearch', count: this.Pagination.itemsPerPage, sorting: { field: "maker_date", sort: "DESC", type: "date" } });

        this.tableContent.sorting.field = '';

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getdata":
                    this.search.show = (res.data.service || res.data.contents) && (res.data.service.length > 0 || res.data.contents.length) ? 1 : 0;
                    this.tableService._concat(res.data.service, true)
                    this.tableContent._concat(res.data.contents, true)
                    break;

                default:
                    break;
            }
        })
    }

    ngOnInit() {
        this.route.params.subscribe(params => {

            this.search.name = params.keywords || '';

            this.search.show = params.keywords ? true : false;

            if (params.keywords && params.keywords != '') {

                this.globals.send({ path: this.token.getdata, token: "getdata", params: { keywords: params.keywords } });

            }
        });

    }
    ngOnChanges(e: SimpleChanges) {
        this.getData();
    }
    getData() {

    }
    ngOnDestroy() {
        this.connect.unsubscribe();
    }
    searchAgain() {
        if (this.search.name != "") {

            this.router.navigate(['/' + this.globals.language.getCode() + '/search/' + this.toSlug._ini(this.search.name)]);

            this.search.name = '';

            document.getElementById('searchAgain').blur();
        }
    }



    public Pagination = {

        maxSize: 5,

        itemsPerPage: 12,

        changeService: (event: PageChangedEvent) => {

            const startItem = (event.page - 1) * event.itemsPerPage;

            const endItem = event.page * event.itemsPerPage;

            this.tableService.data = this.tableService.dataList.slice(startItem, endItem);

            let element = document.getElementById('service-search');
            window.scroll({ top: element.offsetTop, behavior: 'smooth', });
        },

        changeContent: (event: PageChangedEvent) => {
            console.log('bap');

            const startItem = (event.page - 1) * event.itemsPerPage;

            const endItem = event.page * event.itemsPerPage;

            this.tableContent.data = this.tableContent.dataList.slice(startItem, endItem);

            let element = document.getElementById('content-search');

            window.scroll({ top: element.offsetTop + 600, behavior: 'smooth', });
        }


    }


}
