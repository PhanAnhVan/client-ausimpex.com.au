import { Component, OnInit, OnDestroy } from '@angular/core';
import { Globals } from '../../globals';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.css'],

})
export class ContactComponent implements OnInit, OnDestroy {

    public connect;

    fm: FormGroup;

    public company: any;

    public flags: boolean = true;

    public width: number;

    public token: any = {
        addContact: "api/addContact",
        getpageslink: 'api/getPageByLink',

    }
    constructor(
        public globals: Globals,
        public fb: FormBuilder,
        public toastr: ToastrService,
        public route: ActivatedRoute,

    ) {
        this.width = document.body.getBoundingClientRect().width;
        this.contact.fmConfigs();
        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case "addContact":
                    this.flags != this.flags;
                    let type = (res['status'] == 1) ? "success" : (res['status'] == 0 ? "warning" : "danger");
                    this.toastr[type](res['message'], type)
                    if (res.status == 1) {
                        this.contact.fmConfigs();
                    }
                    break;

                case "pagescontact":
                    this.contact.data = res.data;
                    break;

                default:
                    break;
            }
        })
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            let link = params.lang == 'vn' ? 'lien-he' : 'contact';
            this.globals.send({ path: this.token.getpageslink, token: "pagescontact", params: { link: link } });
        })
    }

    ngOnDestroy() {
        if (this.connect) {
            this.connect.unsubscribe();
        }
    }

    public contact = {

        data: <any>{},

        token: "api/addContact",

        flags: <boolean>true,

        skip: <boolean>false,

        fmConfigs: (item: any = "") => {

            item = typeof item === 'object' ? item : {};

            this.fm = this.fb.group({

                name: ['', [Validators.required]],

                email: ['', [Validators.required, Validators.pattern(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)]],

                phone: ['', [Validators.required, Validators.pattern(/^([_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5}))|(\d+$)$/)]],

                subject: ['', [Validators.required]],

                message: ['', [Validators.required]],

            })
        },
        addContact: () => {
            this.contact.skip = false;
            if (this.contact.flags && this.fm.valid) {
                this.contact.flags = !this.contact.flags;
                let data = this.fm.value;
                this.globals.send({ path: this.contact.token, token: "addContact", data: data });
            } else {
                this.contact.skip = true
            }

        }
    }


}
