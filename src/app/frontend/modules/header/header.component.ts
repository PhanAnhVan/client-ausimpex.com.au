import { Component, OnInit, OnDestroy, EventEmitter, Output, HostListener, } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Globals } from '../../../globals';
import { ToslugService } from '../../../services/integrated/toslug.service';


@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css'],
    providers: [ToslugService]
})
export class HeaderComponent implements OnInit, OnDestroy {

    public connect;
    public company: any = {};
    public hovermenu: boolean = true;
    public width: number = window.innerWidth;
    public mutiLang: any = { active: {}, data: [] };
    public path: any = '';
    public color: boolean = false;

    public token: any = {
        company: "api/company",
    }

    @Output("loading") loading = new EventEmitter();
    constructor(

        public globals: Globals,
        public translate: TranslateService,
        public router: Router,
        public toSlug: ToslugService,
        public routerAtc: ActivatedRoute,


    ) {
        if (window['fixmenu']) {
            window['fixmenu']();
        }

        let language = this.globals.language.get(false);
        this.mutiLang.data = this.globals.language.getData();
        if (this.mutiLang.data.length > 0) {
            this.mutiLang.data.filter(item => {
                if (+item.id == +language) {
                    this.mutiLang.active = item
                }
                return item
            })
        };
        this.width = document.body.getBoundingClientRect().width;
        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {
                case 'company':
                    this.company = res.data;
                    break;

                default:
                    break;

            }
        });

    }

    ngOnInit() {

        this.router.events.subscribe(event => {

            if (event instanceof NavigationEnd) {

                this.width > 768 ? '' : this.menuMobile.onMenu(true);
            }
        });

        this.globals.send({ path: this.token.company, token: 'company' })

    }

    ngOnDestroy() {

        this.connect.unsubscribe();
    }


    public menuMobile = {

        show: <boolean>true,

        onMenu: (show: boolean) => {
            this.menuMobile.show = show;
            let elm2 = document.getElementById("menu-mobi");
            let headerCenter = document.getElementById("header-center");
            if (this.menuMobile.show == false) {
                elm2.classList.add("menu-mobi-block");
                elm2.classList.remove("menu-mobi-hidden");
                document.querySelector('.menu').classList.add("navbar-fixed-mobile");
                headerCenter.classList.add("navbar-fixed");

            } else {
                elm2.classList.add("menu-mobi-hidden");
                elm2.classList.remove("menu-mobi-block");
                document.querySelector('.menu').classList.remove("navbar-fixed-mobile");
                var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
                if (currentScroll == 0) {
                    headerCenter.classList.remove("navbar-fixed");
                }
            }
        },
    }
    @HostListener('window:scroll', ['$event']) checkScroll() {
        let headerCenter = document.getElementById("header-center");
        var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
        if (currentScroll > 0) {
            headerCenter.classList.add("navbar-fixed");
        } else {
            this.menuMobile.show == false ? '' : headerCenter.classList.remove("navbar-fixed");
        }
    }
    public search = {
        token: '',

        icon: <boolean>false,

        value: '',

        onSearch: () => {
            this.search.value = this.toSlug._ini(this.search.value.replace(/\s+/g, " ").trim());

            if (this.search.check_search(this.search.value)) {

                this.router.navigate(['/' + this.globals.language.getCode() + '/search/' + this.search.value]);

                document.getElementById('search').blur();
            }

            this.search.value = '';

            this.search.icon = false;
        },

        check_search: (value) => {
            let skip = true;
            skip = (value.toString().length > 0 && value != '' && value != '-' && value != '[' && value != ']' && value != '\\' && value != '{' && value != '}') ? true : false;
            return skip;
        },

        showSearch: () => {
            this.search.icon = !this.search.icon;
        }
    }


    onLanguage = (language_id) => {
        this.mutiLang.data.filter(item => {
            if (+item.id == +language_id) {
                this.mutiLang.active = item
            }
            return item
        })
        this.globals.language.set(language_id, false);
        setTimeout(() => {
            this.router.navigate(['/' + this.mutiLang.active.code])
        }, 500);
        this.loading.emit('');
    }
}
