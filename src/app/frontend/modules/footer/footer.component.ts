import { Component, OnInit, TemplateRef } from "@angular/core";
import { Globals } from "../../../globals";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "../../../../../node_modules/@angular/router";

@Component({
    selector: "app-footer",
    templateUrl: "./footer.component.html",
    styleUrls: ["./footer.component.scss"],
})
export class FooterComponent implements OnInit {
    public connect;
    public company;

    public type = "password";

    public service: any = { data: [] };

    public policy: any = { data: [] };

    public about: any = { data: [] };

    public width: number = 0;


    public token: any = {
        menu: "api/getmenu",

    };
    constructor(
        public globals: Globals,
        public translate: TranslateService,
        public router: Router,

    ) {
        this.width = document.body.getBoundingClientRect().width;
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {

                case "menuFooterService":
                    this.service.data = res.data;
                    break;

                case "menuFooterAbout":
                    this.about.data = res.data;
                    break;

                case "menuFooterPolicy":
                    this.policy.data = res.data;
                    break;


                default:
                    break;
            }
        });
    }

    ngOnInit() {

        this.globals.send({ path: this.token.menu, token: "menuFooterPolicy", params: { position: "policy" } });

        this.globals.send({ path: this.token.menu, token: "menuFooterService", params: { position: "service" } });

        this.globals.send({ path: this.token.menu, token: "menuFooterAbout", params: { position: "about" } })

    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }


    getType = (link, type) => {

        switch (+type) {
            case 1:
            case 2:
            case 3:
            case 4:
                link = link;
                break;

            default:
                break;
        }

        return link;
    };



}
