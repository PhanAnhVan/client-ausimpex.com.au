import { Component, Input, OnInit } from '@angular/core';
import { Globals } from '../../../globals';

@Component({
    selector: 'app-box-gallery',
    templateUrl: './box-gallery.component.html',
    styleUrls: ['./box-gallery.component.css']
})
export class BoxGalleryComponent implements OnInit {

    @Input('item') item: any;

    constructor(public globals: Globals) { }

    ngOnInit() {
    }

}
