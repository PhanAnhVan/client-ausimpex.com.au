import { Component, OnInit, OnDestroy } from '@angular/core';
import { Globals } from '../../../globals';
import { Router, NavigationEnd } from '@angular/router';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.css'],

})
export class MenuComponent implements OnInit, OnDestroy {

    private connect;
    public menu: any;
    public link: string;
    public width: number = window.innerWidth;
    public path: any = '';
    public color: boolean = false;
    public token: any = {
        menu: "api/getmenu",
    }
    constructor(
        public globals: Globals,
        public router: Router,
    ) {

        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                window.scroll(0, 0);
                this.link = window.location.pathname
            }
        })
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getMenuMain":
                    this.menu = this.compaid(res.data);
                    break;
                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.globals.send({ path: this.token.menu, token: 'getMenuMain', params: { position: 'menuMain' } });
        // this.router.events.subscribe((params: any) => {
        //     this.routerPage();
        // });
    }

    compaid(data) {
        let list = [];
        data = data.filter(function (item) {
            let v = (isNaN(+item.parent_id) && item.parent_id) ? 0 : +item.parent_id;
            v == 0 ? '' : list.push(item);
            return v == 0 ? true : false;
        })
        let compaidmenu = (data, skip, level = 0) => {
            level = level + 1;
            if (skip == true) {
                return data;
            } else {
                for (let i = 0; i < data.length; i++) {
                    let obj = data[i]['data'] && data[i]['data'].length > 0 ? data[i]['data'] : []
                    list = list.filter(item => {
                        let skip = (+item.parent_id == +data[i]['id']) ? false : true;
                        if (skip == false) { obj.push(item); }
                        return skip;
                    })
                    let skip = (obj.length == 0) ? true : false;
                    data[i]['href'] = getType(data[i]['link'], +data[i].type);
                    data[i]['level'] = level;
                    data[i]['data'] = compaidmenu(obj, skip, level);
                }
                return data;
            }
        };
        let getType = (link, type) => {
            switch (+type) {
                case 1:
                case 2:
                case 4:
                case 3:
                    link = '/' + link
                    break;
                default:
                    break;
            }
            return link;
        }
        return compaidmenu(data, false);
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    // routerPage() {
    //     this.path = window.location.pathname;
    //     switch (this.path) {
    //         case '/vn':
    //         case '/en':
    //             this.color = false;
    //             break;

    //         default:
    //             this.color = true;
    //             break;
    //     }
    // }


}
