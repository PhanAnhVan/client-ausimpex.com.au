import { Component, OnInit, OnDestroy, } from '@angular/core';
import { Globals } from '../../../globals';
import { OwlOptions, SlidesOutputData } from "ngx-owl-carousel-o";

@Component({
    selector: 'app-slide',
    templateUrl: './slide.component.html',
    styleUrls: ['./slide.component.scss'],
})
export class SlideComponent implements OnInit, OnDestroy {
    private connect;

    public activeSlides: SlidesOutputData;

    public selected: any;

    public width: number = document.body.getBoundingClientRect().width;

    constructor(
        public globals: Globals

    ) {
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response.token) {
                case "getslide":
                    this.slide.data = response.data;
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.slide.send();
    }

    ngOnDestroy() {
        if (this.connect) {
            this.connect.unsubscribe();
        }
    }

    /** SLIDE **/
    public slide = {
        token: "api/home/slide",
        data: [],
        send: () => {
            this.globals.send({ path: this.slide.token, token: "getslide", params: { type: 1 } });
        },
    };


    public slideOptions: OwlOptions = {
        loop: true,
        mouseDrag: true,
        touchDrag: false,
        pullDrag: false,
        navSpeed: 500,
        autoplayTimeout: 10000,
        autoplaySpeed: 1000,
        autoplay: false,
        items: 1,
        dots: true,
        nav: true,
        navText: [
            "<img src='../../../../assets/img/left-chevron.png'>",
            "<img src='../../../../assets/img//right-arrow.png'>"
        ],
    };

    getPassedData(data: SlidesOutputData) {
        this.activeSlides = data;
        this.selected = data.slides[0] ? data.slides[0].id : "";
        let items = document.querySelectorAll('.slide-content');
        for (let i = 0; i < items.length; i++) {
            items[i].classList.add("bounceInRight");
        }
        setTimeout(() => {
            for (let i = 0; i < items.length; i++) {
                items[i].classList.remove("bounceInRight");
            }
        }, 1000);

    }
}
