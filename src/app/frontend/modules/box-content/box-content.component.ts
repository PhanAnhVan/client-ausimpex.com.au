import { Component, Input } from '@angular/core';
import { Globals } from '../../../globals';

@Component({
    selector: 'app-box-content',
    templateUrl: './box-content.component.html',
    styleUrls: ['./box-content.component.scss'],
})
export class BoxContentComponent {
    @Input('item') item: any;
    @Input('type') type: number = 0;
    constructor(public globals: Globals) { }

    ngOnInit() { }
}