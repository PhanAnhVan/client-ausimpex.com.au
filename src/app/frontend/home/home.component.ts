import { Component, OnInit, OnDestroy, } from '@angular/core';
import { Globals } from '../../globals';
import { Router } from '@angular/router';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],

})
export class HomeComponent implements OnInit, OnDestroy {

    private connect;
    public width: number = document.body.getBoundingClientRect().width;
    constructor(
        public globals: Globals,
        public router: Router,

    ) {

        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response.token) {
                case 'getAboutus':
                    this.about.data = response.data;
                    break;

                case 'getContentHome':
                    this.content.data = response.data;
                    break


                case "getProductGroup":
                    this.product.group = response.data;
                    if (this.product.group && this.product.group.list.length > 0) {
                        this.product.getProductList(this.product.group.list[0].id, this.product.group.list[0].link);
                    }
                    break;
                case "getProductList":
                    this.product.data = response.data// this.product.compareData(response.data);
                    break;

                default:
                    break;
            }
        })
    }

    ngOnInit() {
        this.product.send();
        this.about.send();
        this.content.send();
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }
    public lengthItem = 0;
    private token = {
        productGroup: "api/home/getproductgroup",
        productList: "api/getProductList",
    }
    public product = {
        group: <any>{},
        data: [],
        show: 0,
        link: '',
        send: () => {
            this.globals.send({
                path: this.token.productGroup,
                token: "getProductGroup",
            })
        },
        getProductList: (idGroup: number, link = '') => {
            this.product.link = link
            if (idGroup && idGroup > 0) {
                this.globals.send({
                    path: this.token.productList,
                    token: "getProductList",
                    params: { limit: 20, id: idGroup }
                })
            }
        },
        compareData: (listCompare: any[]) => {
            let list = [];
            let length = listCompare.length / 2;
            this.lengthItem = length;
            for (let i = 0; i < length; i++) {
                list[i] = [];
                if (list[i].length < 2) {
                    list[i] = listCompare.splice(0, 2);
                }
                listCompare = Object.values(listCompare);
            }
            return list;
        },
        options: {
            loop: false,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            autoplayTimeout: 8000,
            autoplaySpeed: 1500,
            autoplay: false,
            margin: 10,
            responsive: {
                0: {
                    items: 2,
                    stagePadding: this.lengthItem <= 2 ? 0 : 50,
                },
                415: {
                    items: 3,
                    stagePadding: this.lengthItem <= 2 ? 0 : 100,
                },
                940: {
                    items: 5,
                    stagePadding: this.lengthItem <= 5 ? 0 : 200,
                },
            },
            dots: false,
            nav: true,
            navText: [
                '<img src="../../../../assets/img/icon-slide-left.png" alt="Arrow left" class="img-fluid" />',
                '<img src="../../../../assets/img/icon-slide-right.png" alt="Arrow right" class="img-fluid" />',
            ],
        },
    };

    public about = {
        token: 'api/getAboutus',
        data: <any>{},
        send: () => {
            this.globals.send({ path: this.about.token, token: "getAboutus" });
        },
    }

    public content = {
        token: 'api/getContentHome',
        data: <any>{},
        send: () => {
            this.globals.send({ path: this.content.token, token: "getContentHome", params: { limit: 4 } });
        },
    }
}