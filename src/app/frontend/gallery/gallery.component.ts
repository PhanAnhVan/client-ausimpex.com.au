import { Component, OnInit, Input } from '@angular/core';
import { Globals } from '../../globals';
import { DomSanitizer } from '@angular/platform-browser';
import { TableService } from '../../services/integrated/table.service';
import { PageChangedEvent } from 'ngx-bootstrap';


@Component({
    selector: 'app-gallery',
    templateUrl: './gallery.component.html',
    styleUrls: ['./gallery.component.css'],
    providers: [TableService]

})
export class GalleryComponent implements OnInit {

    @Input('data') data: any;

    constructor(
        public globals: Globals,
        public sanitizer: DomSanitizer,
        public tableLibrary: TableService,
    ) {


    }

    ngOnInit() {
        setTimeout(() => {
            this.library._ini(this.data);
        }, 200);
    }

    library = {
        number: 0,
        data: [],
        listImages: [],
        listVideo: [],

        _ini: (data) => {
            this.library.data = data;
            this.library.data.reduce((n, o, i) => {
                o.value = (o.type == 2) ? this.sanitizer.bypassSecurityTrustResourceUrl(o.value.replace('watch?v=', 'embed/')) : o.value;
                (o.type == 2) ? this.library.listVideo.push(o) : this.library.listImages.push(o);
                return n;
            }, []);

            this.tableLibrary._ini({ data: this.library.listImages, count: 12, pages: 1 });
            this.tableLibrary._concat(this.library.listImages, true);

            setTimeout(() => {
                if (window["viewLibrary"]) {
                    window["viewLibrary"]();
                }

                if (window["libraryGallary"]) {
                    window["libraryGallary"]();
                }
            }, 500);
        },

        filter: (skip) => {
            this.library.number = (skip == 1) ? 1 : (skip == 2 ? 2 : 0);
        }
    }


    public Pagination = {

        maxSize: 5,

        itemsPerPage: 20,

        change: (event: PageChangedEvent) => {

            const startItem = (event.page - 1) * event.itemsPerPage;

            const endItem = event.page * event.itemsPerPage;

            this.tableLibrary.data = this.tableLibrary.cached.slice(startItem, endItem);

            var el = document.getElementById('ListData');

            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        }
    }
}
