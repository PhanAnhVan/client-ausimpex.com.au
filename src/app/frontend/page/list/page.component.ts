import { Component, OnDestroy, OnInit, } from '@angular/core';
import { Globals } from '../../../globals';
import { ActivatedRoute, Router } from '@angular/router';
import { TableService } from '../../../services/integrated/table.service';
import { PageChangedEvent } from 'ngx-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-page',
    templateUrl: './page.component.html',
    styleUrls: ['./page.component.css'],

})
export class ListPageComponent implements OnInit, OnDestroy {

    private connect;
    public data: any = {};
    public pageView: number = -1;
    public token: any = {
        getPage: "api/getPageByLink",
        content: "api/page/content"
    }

    public page = new TableService();

    constructor(
        public globals: Globals,
        public route: ActivatedRoute,
        public router: Router,
        public sanitizer: DomSanitizer,
    ) {
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response.token) {
                case "getPage":
                    this.data = {};
                    this.data = response.data;                    
                    if (Object.keys(this.data).length > 0) {
                        let type = response.data.type || 0;
                        switch (+type) {
                            case 2:
                                setTimeout(() => {
                                    this.pageView = response.data.length > 0 ? 1 : 0;
                                }, 100);
                                break;
                            case 3:
                                this.products.send(response.data.id)
                                break;
                            case 4:
                                this.contents.send(response.data.id)
                                break;
                            default:
                                break;
                        }
                    }

                    break;
                case "getDataProducts":
                    this.products.data = response.data;
                    setTimeout(() => {
                        this.pageView = response.data.length > 0 ? 1 : 0;
                    }, 100);
                    break;
                case "getDataContents":
                    this.contents.data = response.data;
                    setTimeout(() => {
                        this.pageView = response.data.length > 0 ? 1 : 0;
                    }, 100);
                    break;
                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.page._ini({ data: [], keyword: 'getPage', count: this.Pagination.itemsPerPage, sorting: { field: "maker_date", sort: "DESC", type: "date" } });
        this.route.params.subscribe(params => {
            let link = params.link;
            this.globals.send({ path: this.token.getPage, token: "getPage", params: { link: link || '' } });
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }
    public products = {
        token: "api/page/products",
        data: [],
        send: (id) => {
            this.globals.send({ path: this.products.token, token: "getDataProducts", params: { id: id || 0 } });
        }
    }

    public contents = {
        token: "api/page/contents",
        data: [],
        send: (id) => {
            this.globals.send({ path: this.contents.token, token: "getDataContents", params: { id: id || 0 } });
        }
    }
    public Pagination = {

        maxSize: 5,

        itemsPerPage: 15,


        change: (event: PageChangedEvent) => {

            const startItem = (event.page - 1) * event.itemsPerPage;

            const endItem = event.page * event.itemsPerPage;

            this.page.data = this.page.cached.slice(startItem, endItem);

            var el = document.getElementById('ListData');

            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        }
    }

    renderHtml = () => {
        let main = document.getElementById("contentDetail");
        if (main) {
            let el = main.querySelectorAll("table"); 7
            if (el) {
                for (let i = 0; i < el.length; i++) {
                    let div = document.createElement("div");
                    div.className = "table-responsive table-bordered m-0 border-0";
                    el[i].parentNode.insertBefore(div, el[i]);
                    el[i].className = 'table';
                    el[i].setAttribute('class', 'table');
                    let cls = el[i].getAttribute('class');
                    el[i]
                    let newhtml = "<table class='table'>" + el[i].innerHTML + "</table>";
                    el[i].remove();
                    div.innerHTML = newhtml;
                }
            }
            let image = main.querySelectorAll("img");
            if (image) {
                for (let i = 0; i < image.length; i++) {
                    let a = document.createElement("div");
                    a.className = "images-deatil d-inline";
                    image[i].parentNode.insertBefore(a, image[i]);
                    let style = image[i].style.cssText
                    let src = image[i].currentSrc
                    let html = `<a  class="fancybox" data-fancybox="images-preview" data-thumbs="{&quot;autoStart&quot;:true}" href="` + src + `">
                         <img class=""  src="`+ src + `" style="` + style + `" alt="` + this.data.name + `">
                    </a>`
                    image[i].remove()
                    a.innerHTML = html;
                }
            }
        }
    }

}
