import { Component, OnDestroy, OnInit, } from '@angular/core';
import { Globals } from '../../../globals';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-detail-page',
    templateUrl: './detail-page.component.html',
    styleUrls: ['./detail-page.component.scss'],

})
export class DetailPageComponent implements OnInit, OnDestroy {
    public connect;
    public data = <any>{};
    public link: string = '';
    public width;
    public page = <any>{};
    public token: any = {
        getDetail: "api/pages/detail",
    }
    constructor(
        public globals: Globals,
        public fb: FormBuilder,
        public route: ActivatedRoute,
        public router: Router,
    ) {
        this.width = document.body.getBoundingClientRect().width;
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response.token) {
                case "getDetail":
                    this.data = response.data;
                    setTimeout(() => {
                        this.renderHtml();
                    }, 500);
                    break;
                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            setTimeout(() => {
                this.globals.send({ path: this.token.getDetail, token: "getDetail", params: { link: params.link, parent_link: params.parent_link } });
            }, 50);
        });

    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    renderHtml = () => {
        let main = document.getElementById("contentDetail");
        if (main) {
            let el = main.querySelectorAll("table");
            if (el) {
                for (let i = 0; i < el.length; i++) {
                    let div = document.createElement("div");
                    div.className = "table-responsive table-bordered m-0 border-0";
                    el[i].parentNode.insertBefore(div, el[i]);
                    el[i].className = 'table';
                    el[i].setAttribute('class', 'table');
                    let cls = el[i].getAttribute('class');
                    el[i]
                    let newhtml = "<table class='table'>" + el[i].innerHTML + "</table>";
                    el[i].remove();
                    div.innerHTML = newhtml;
                }
            }
            let image = main.querySelectorAll("img");
            if (image) {
                for (let i = 0; i < image.length; i++) {
                    let a = document.createElement("div");
                    a.className = "images-deatil d-inline";
                    image[i].parentNode.insertBefore(a, image[i]);
                    let style = image[i].style.cssText
                    let src = image[i].currentSrc
                    let html = `<a  class="fancybox" data-fancybox="images-preview" data-thumbs="{&quot;autoStart&quot;:true}" href="` + src + `">
                         <img class=""  src="`+ src + `" style="` + style + `" alt="` + this.data.name + `">
                    </a>`
                    image[i].remove()
                    a.innerHTML = html;
                }
            }
        }
    }
}