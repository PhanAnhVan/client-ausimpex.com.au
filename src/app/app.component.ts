import { Component, OnInit, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Meta, Title } from '@angular/platform-browser';
import { Globals } from './globals';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
})
export class AppComponent implements OnInit, OnDestroy {
	public connect;
	public loading: boolean = false;
	public token = {
		getcompany: "api/company",
		mutiLanguage: 'api/mutiLanguage'
	}
	constructor(
		public translate: TranslateService,
		private globals: Globals,
		private title: Title,
		private meta: Meta,
		public router: Router,
	) {

		this.translate.setDefaultLang('vi');
		this.translate.use('vn');

		this.globals.send({ path: this.token.mutiLanguage, token: 'mutiLang' })
		this.globals.send({ path: this.token.getcompany, token: 'getcompany' })
		this.connect = this.globals.result.subscribe((response: any) => {
			switch (response.token) {
				case 'getcompany':
					if (Object.keys(response.data).length > 0) {
						let data = response.data;
						this.title.setTitle(data.name);
						this.meta.addTag({ name: 'description', content: data.description });
						this.meta.addTag({ name: 'keywords', content: data.keywords });
						let shortcut = document.querySelector("[type='image/x-icon']");
						if (shortcut && data.shortcut) { shortcut.setAttribute('href', data.shortcut); }
					}
					else { this.globals.send({ path: this.token.getcompany, token: 'getcompany' }) };

					break;

				case 'mutiLang':
					let data = response.data;
					if (data && data.length > 0) {
						let languageCode = [];
						response.data.reduce((n, o, i) => { languageCode.push(o.code); return n }, [])
						this.translate.addLangs(languageCode);
						this.globals.language.setData(data);
					} else {
						this.globals.send({ path: this.token.mutiLanguage, token: 'mutiLang' })
					}
					break;
				default:
					break;
			}
		})
	}
	ngOnInit() {
	}
	ngOnDestroy() {
		this.connect.unsubscribe();
	}
}
