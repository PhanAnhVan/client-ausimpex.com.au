function viewLibrary() {
    $('.fancybox').fancybox({
        thumbs: { "autoStart": true }
    });
}
function libraryGallary() {
    var $container = $(".animate-grid .gallary-thumbs");
    $container.isotope({
        filter: "*",
        animationOptions: {
            duration: 750,
            easing: "linear",
            queue: false,
        },
    });
    $(".animate-grid .categories a").click(function () {
        $(".animate-grid .categories .active").removeClass("active");
        $(this).addClass("active");
        var selector = $(this).attr("data-filter");
        $container.isotope({
            filter: selector,
            animationOptions: {
                duration: 750,
                easing: "linear",
                queue: false,
            },
        });
        return false;
    });
}

function scrolltop() {
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 200) {
            $('.scroll-top').fadeIn(200);
        } else {
            $('.scroll-top').fadeOut(200);
        }
    });
    $('.scroll-top').on("click", function () {
        $("html,body").animate({ scrollTop: 0 }, 1500);
        return false;
    });
}


$(document).ready(function () {
    $("body").tooltip({ selector: '[data-toggle=tooltip]' });
});